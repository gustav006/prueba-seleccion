module ApplicationHelper
 
    require 'uri'
    require 'net/http'
    require 'json'
      
    def get_data_json(url)
      url = url.gsub(" ","")
      escaped_address = URI.escape(url) 
      uri = URI.parse(url)
  
      if uri.kind_of?(URI::HTTP) or uri.kind_of?(URI::HTTPS)
        result = Net::HTTP.get_response(uri)
        if valid_json?(result.body)
          result = JSON.parse(result.body)
          #puts " RESULT : #{result}"
        else  
          return false  
        end
        if !result.nil? && !result['success'].nil? && result['success'] == 1 
          result['book'].each do |character|
            puts "NAME :  #{character['name']}"
            puts "GENDER    : #{character['gender']}"
            puts "SLUG    : #{character['slug']}"
            puts "RANK    : #{character['pagerank']['rank']}"
            puts "HOUSE    : #{character['house']}"
            puts "BOOKS    : #{character['books']}"
            puts "TITTLES    : #{character['titles']}"
            puts ""
            puts ""
            Character.create(name: character['name'], gender: character['gender'], slug: character['slug'],
             rank: character['pagerank']['rank'], house: character['house'])
            #!item['comments'].nil? ? Comment.create(content: item['comments'], item_id: i.id) : ''
            #if !item['categories'].nil? 
            #  item['categories'].each do |cat| ## validar esto 
            #    c = Category.where(name: cat['name'] )
            #    if c.empty?
            #      c = Category.create(name: cat['name'], slug: cat['slug'])
            #      !i.errors.messages.any? ?  CategoriesItem.create( category_id: c.id, item_id: i.id) : ''       
            #    else
            #      !i.errors.messages.any? ?  CategoriesItem.create( category_id: c[0].id, item_id: i.id) : ''
            #    end
            #  end
            #end
          end
        end
      end
    end
  
    def valid_json?(string)
      begin
        !!JSON.parse(string)
      rescue JSON::ParserError
        false
      end
    end
  end
  