class CreateCharacters < ActiveRecord::Migration[5.2]
  def change
    create_table :characters do |t|
      t.string :name
      t.string :gender
      t.string :slug
      t.string :rank
      t.string :house

      t.timestamps
    end
  end
end
